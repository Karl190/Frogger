package resources;

public class Goal extends GameObject implements Hitable {
	
	private boolean reached;
	public Goal(int posx, int posy) {
		super(posx, posy, ObjectNames.GOAL);
		this.reached=false;
	}

	@Override
	public boolean onHit() {
		this.reached=true;
		return false;
	}

	public boolean isReached() {
		return reached;
	}

}
