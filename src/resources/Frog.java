package resources;

import controller.GameController;

public class Frog extends GameObject{
	
	boolean dragged;

	public Frog(int posx, int posy) {
		super(posx, posy, ObjectNames.FROG);
	}

	public void move(Direction di) {
		switch(di) {
		case DOWN:
			this.setPosy(this.getPosy()+1);
			break;
		case LEFT:
			this.setPosx(this.getPosx()-1);
			break;
		case RIGHT:
			this.setPosx(this.getPosx()+1);
			break;
		case UP:
			this.setPosy(this.getPosy()-1);
			break;
		default:
			break;
		}
		if(this.getPosy()<0) {
			this.setPosy(0);
		}
		else if(this.getPosy()>GameController.Matrixsizey-1) {
			this.setPosy(GameController.Matrixsizey-1);
		}
		else if(this.getPosx()<0) {	
			this.setPosx(0);
		}
		else if(this.getPosx()>GameController.Matrixsizex-1) {
			this.setPosx(GameController.Matrixsizex-1);
		}
	}

	public boolean isDragged() {
		return dragged;
	}

	public void setDragged(boolean dragged) {
		this.dragged = dragged;
	}
	
}
