package displays;

import java.io.IOException;
import java.util.ArrayList;

import frontend.Input;
import resources.LogicalField;

public interface DisplayAble {
	public void printMatrix(ArrayList<ArrayList<ArrayList<LogicalField>>> matrix);
	public Character getMenuInput() throws IOException;
	public void printText(String str);
	public void clear();
	public int[] getConfigInput() throws NumberFormatException, IOException;
	public Input getInput() throws IOException;
	public void printError(String string);
}
