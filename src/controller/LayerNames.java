package controller;

public enum LayerNames {
	GROUND(0),MOVING(1),FROG(2);
	
	private int numVal;
	
	LayerNames(int numVal){
		this.numVal=numVal;
	}

	public int getNumVal() {
		return numVal;
	}
	
}
