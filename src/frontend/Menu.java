package frontend;

import java.io.IOException;

import displays.DisplayAble;

public class Menu {
	
	DisplayAble dis;
	
	public Menu(DisplayAble dis) {
		this.dis=dis;
	}

	public Input mainMenu() throws IOException {
		Character in;
		dis.clear();
		dis.printText(("Welcome to the Main Menu"));
		dis.printText(("[1] Start"));
		dis.printText(("[2] Configuration"));
		dis.printText(("[3] Close"));
		do {
			in=dis.getMenuInput();
		}while(in==null);
		switch(in) {
		case '1':
			return Input.START;
		case '2':
			return Input.CONFIG;
		case '3':
			return Input.END;
		}
		return null;
	}

	public Input configMenu() throws IOException {
		Character in;
		dis.clear();
		dis.printText(("Welcome to the Config Menu"));
		dis.printText(("[1] Configure"));
		dis.printText(("[2] Load"));
		dis.printText(("[3] Back"));
		do {
			in=dis.getMenuInput();
		}while(in==null);
		switch(in) {
		case '1':
			return Input.CONFIG;
		case '2':
			return Input.LOAD;
		case '3':
			return Input.CONTINUE;
		}
		return null;
	}
	public Input pauseMenu() throws IOException {
		Character in;
		dis.clear();
		dis.printText(("Welcome to the Pause Menu"));
		dis.printText(("[1] Continue"));
		dis.printText(("[2] Back to Main Menu"));
		dis.printText(("[3] End"));
		do {
			in=dis.getMenuInput();
		}while(in==null);
		switch(in) {
		case '1':
			return Input.CONTINUE;
		case '2':
			return Input.RESTART;
		case '3':
			return Input.END;
		}
		return null;
	}
}
